object HelloWorld {
  def main(args: Array[String]): Unit = {
    println("Hello world!")
  }

  def headOfList[T](list: List[T]): T  = {
    list.head
  }
}
