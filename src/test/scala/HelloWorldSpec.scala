import org.scalatest._

class HelloWorldSpec extends FlatSpec with Matchers {
  import HelloWorld._
  "HelloWorld" should "have tests" in {
    true should === (true)
  }

  "Head of non-empty list" should "return value" in {
    headOfList(2::2::Nil) should === (2)
  }

  "Head of empty list" should "throw NoSuchElementException" in {
    intercept[NoSuchElementException] {
      headOfList(Nil)
    }
  }

}
